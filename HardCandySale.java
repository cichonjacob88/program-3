/*
* File name: HardCandySale.java
*
* Programmer: Jacob Cichon
* ULID: JVCICHO
*
* Date: Oct 10, 2018
*
* Class: IT 168
* Lecture Section: 19
* Lecture Instructor: Dr. Karen Johnson
* Lab Section: 21
* Lab Instructor: Kumar Rakholia
*/
package edu.ilstu;

/**
* Processes orders for Hard Candies
*
* @author Jacob Cichon
*
*/
public class HardCandySale
{
	final double peanutBrittle = 11.00;
	final double peppermint = 8.00;
	final double tier1 = 0.07;
	final double tier2 = 0.12;
	int numOfPeanutBrittle;
	int numOfPeppermint;
	public int totalHardCandy = numOfPeanutBrittle + numOfPeppermint;
	
	/**
	 * Gets Peanut Brittle price
	 * @return peanutBrittle
	 */
	
	
	
	public double getPeanutBrittle()
	{
		return peanutBrittle;
	}
	
	/**
	 * Gets Total Hard Candy
	 * @return the totalHardCandy
	 */
	public int getTotalHardCandy()
	{
		return totalHardCandy;
	}

	/**
	 * @param totalHardCandy the totalHardCandy to set
	 */
	public void setTotalHardCandy(int totalHardCandy)
	{
		this.totalHardCandy = totalHardCandy;
	}

	/**
	 * Gets peppermint price
	 * @return peppermint
	 */
	
	public double getPeppermint()
	{
		return peppermint;
	}
	
	/**
	 * Gets Tier 1 percentage
	 * @return tier1
	 */
	
	public double getTier1()
	{
		return tier1;
	}
	
	/**
	 * Gets Tier 2 percentage
	 * @return tier2
	 */
	
	public double getTier2()
	{
		return tier2;
	}
	
	/**
	 * Gets number of Peanut Brittle
	 * @return numOfPeanutBrittle
	 */
	
	public int getNumOfPeanutBrittle()
	{
		return numOfPeanutBrittle;
	}
	
	/**
	 * Gets number of Peppermint
	 * @return numOfPeppermint
	 */
	
	public int getNumOfPeppermint()
	{
		return numOfPeppermint;
	}

	/**
	 * Sets number of Peanut Brittle
	 * @param numOfPeanutBrittle
	 */
	
	public void setNumOfPeanutBrittle(int numOfPeanutBrittle)
	{
		this.numOfPeanutBrittle = numOfPeanutBrittle;
	}

	/**
	 * Sets number of Peppermints
	 * @param numOfPeppermint
	 */
	
	public void setNumOfPeppermint(int numOfPeppermint)
	{
		this.numOfPeppermint = numOfPeppermint;
	}
	
	/**
	 * Calculates the total cost of Hard Candies
	 * @param peanutBrittle, numOfPeanutBrittle, peppermint, numOfPeppermint
	 * @return totalCost
	 */
	
		
	public double calculateHardCandyCost() {
		double totalCost;
		totalCost = (peanutBrittle * numOfPeanutBrittle) + (peppermint * numOfPeppermint);
		return totalCost;
				
		
		}
}
