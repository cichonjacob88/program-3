/*
* File name: SoftCandySale.java
*
* Programmer: Jacob Cichon
* ULID: JVCICHO
*
* Date: Oct 10, 2018
*
* Class: IT 168
* Lecture Section: 19
* Lecture Instructor: Dr. Karen Johnson
* Lab Section: 21
* Lab Instructor: Kumar Rakholia
*/
package edu.ilstu;

/**
* Processes orders for Soft Candies
*
* @author Jacob Cichon
*
*/
public class SoftCandySale
{
	final double taffy = 7.00;
	final double chocolate = 25.00;
	final double gummiBears = 12.00;
	final double tier1 = 0.07;
	final double tier2 = 0.12;
	public int numOfTaffy;
	public int numOfChocolate;
	public int numOfGummiBears;
	public int totalSoftCandy = numOfTaffy + numOfChocolate + numOfGummiBears;
	/**
	 * Gets Taffy price
	 * @return taffy
	 */
	
	
	
	public double getTaffy()
	{
		return taffy;
	}
	
	/**
	 * Gets Total Soft Candy
	 * @return the totalSoftCandy
	 */
	public int getTotalSoftCandy()
	{
		return totalSoftCandy;
	}

	/**
	 * Sets Total Soft Candy
	 * @param totalSoftCandy the totalSoftCandy to set
	 */
	public void setTotalSoftCandy(int totalSoftCandy)
	{
		this.totalSoftCandy = totalSoftCandy;
	}

	/**
	 * Gets chocolate price
	 * @return chocolate
	 */
	
	public double getChocolate()
	{
		return chocolate;
	}
	
	/**
	 * Gets gummi bear price
	 * @return gummiBear
	 */
	
	public double getGummiBears()
	{
		return gummiBears;
	}
	
	/**
	 * Gets Tier 1 percentage
	 * @return tier1
	 */
	
	public double getTier1()
	{
		return tier1;
	}
	
	/**
	 * Gets Tier 2 percentage
	 * @return tier2
	 */
	
	public double getTier2()
	{
		return tier2;
	}
	
	/**
	 * Gets number of taffy
	 * @return numOfTaffy
	 */
	
	public int getNumOfTaffy()
	{
		return numOfTaffy;
	}
	
	/**
	 * Gets number of chocolate
	 * @return numOfChocolate
	 */
	
	public int getNumOfChocolate()
	{
		return numOfChocolate;
	}
	
	/**
	 * Gets number of gummi bears
	 * @return numOfGummiBears
	 */
	
	public int getNumOfGummiBears()
	{
		return numOfGummiBears;
	}

	/**
	 * Sets number of Taffy
	 * @param numOfTaffy
	 */
	
	public void setNumOfTaffy(int numOfTaffy)
	{
		this.numOfTaffy = numOfTaffy;
	}

	/**
	 * Sets number of chocolate
	 * @param numOfchocolate
	 */
	
	public void setNumOfChocolate(int numOfChocolate)
	{
		this.numOfChocolate = numOfChocolate;
	}
	
	/**
	 * Sets number of gummi bears
	 * @param numOfGummiBears
	 */
	
	public void setNumOfGummiBears(int numOfGummiBears)
	{
		this.numOfGummiBears = numOfGummiBears;
	}
	
	/**
	 * Calculates the total cost of Soft Candies
	 * @param taffy, numOfTaffy, chocolate, numOfChocolate, gummiBears, numOfGummiBears
	 * @return softCandyCost
	 */
	
	public double calculateSoftCandyCost() {
		double totalCost= ((taffy * getNumOfTaffy()) + (chocolate * numOfChocolate) + (gummiBears * numOfGummiBears));
		totalCost=Math.round(totalCost*100.0)/100.0;
		return totalCost;
	}
}
