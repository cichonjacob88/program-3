/*
* File name: CandySaleApp.java
*
* Programmer: Jacob Cichon
* ULID: JVCICHO
*
* Date: Oct 10, 2018
*
* Class: IT 168
* Lecture Section: 19
* Lecture Instructor: Dr. Karen Johnson
* Lab Section: 21
* Lab Instructor: Kumar Rakholia
*/
package edu.ilstu;
import java.text.DecimalFormat;
import java.util.Scanner;
/**
* Calculates profits and displays final information
*
* @author Jacob Cichon
*
*/
public class CandySaleApp
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
	
		HardCandySale hard = new HardCandySale();
		SoftCandySale soft = new SoftCandySale();
		DecimalFormat currency = new DecimalFormat("0.00");	
		String newLine = System.getProperty("line.separator");
		
		
		System.out.println("\t" + "Welcome to the Candy Shop!" + newLine);
		System.out.println("Hard Candy" + newLine);
		System.out.println("\t" + "Peanut Brittle" + "\t\t" + "$" + currency.format(hard.getPeanutBrittle()));
		System.out.println("\t" + "Peppermint" + "\t\t" + "$" + currency.format(hard.getPeppermint()) + newLine);
		System.out.println("Soft Candy" + newLine);
		System.out.println("\t" + "Taffy" + "\t\t\t" + "$" + currency.format(soft.getTaffy()));
		System.out.println("\t" + "Chocolate" + "\t\t" + "$" + currency.format(soft.getChocolate()));
		System.out.println("\t" + "Gummi Bears" + "\t\t" + "$" + currency.format(soft.getGummiBears()) + newLine + newLine);
		
		
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter number of Peanut Brittle Hard Candy:");
		hard.setNumOfPeanutBrittle( keyboard.nextInt());
		System.out.print("Enter number of Peppermint Hard Candy:");
		hard.setNumOfPeppermint( keyboard.nextInt());
		System.out.print("Enter number of Taffy Soft Candy:");
		soft.setNumOfTaffy( keyboard.nextInt());
		System.out.print("Enter number of Chocolate Soft Candy:");
		soft.setNumOfChocolate( keyboard.nextInt());
		System.out.print("Enter number of Gummi Bears soft Candy:");
		soft.setNumOfGummiBears( keyboard.nextInt());
		keyboard.close();
		
		int totalHardCandy = hard.getNumOfPeanutBrittle() + hard.getNumOfPeppermint();
		int totalSoftCandy = soft.getNumOfChocolate() + soft.getNumOfTaffy() + soft.getNumOfGummiBears();
		
		System.out.println(newLine);
		System.out.println("Order Summary");
		System.out.println("\t" + "Candy Sale");
		System.out.println("Hard Candy ordered:" + totalHardCandy);
		System.out.println("Soft Candy ordered:" + totalSoftCandy + newLine);
		System.out.println("Hard Candy sale: $" + currency.format(hard.calculateHardCandyCost()));
		System.out.println("Soft Candy sale: $" + currency.format(soft.calculateSoftCandyCost()) + newLine);
		System.out.println("School Profit: $" + (CandySaleApp.calculateHardCandySchoolProfit() + CandySaleApp.calculateSoftCandySchoolProfit()));
		System.out.println("Total Sale for School: $" + currency.format(calculateTotalNetCost()));
	}
		
		static int totalHardCandy = hard.getNumOfPeanutBrittle() + hard.getNumOfPeppermint();
		static int totalSoftCandy = soft.getNumOfChocolate() + soft.getNumOfTaffy() + soft.getNumOfGummiBears();
		
		public static double calculateHardCandySchoolProfit() {
			  double totalCost;
			    if (hard.getTotalHardCandy() <= 9 && hard.getTotalHardCandy() >= 1) {
			    totalCost = (hard.calculateHardCandyCost() * hard.getTier1());
			   			    }
			    else {
			     totalCost = (hard.calculateHardCandyCost() * hard.getTier2());
			    }
			     return totalCost;
			  }
		 
			    
		public static double calculateSoftCandySchoolProfit() {
				    double totalCost;
					if (soft.getTotalSoftCandy() <= 7 && soft.getTotalSoftCandy() > 1) {
				    totalCost = (soft.calculateSoftCandyCost() * soft.getTier1());
				  				    }
				   else {
				    totalCost = (soft.calculateSoftCandyCost() * soft.getTier2());
				   }
				    return totalCost;
				      }
						  
		public static double calculateTotalNetCost() {
			double totalCost;
			double cost = soft.calculateSoftCandyCost() + hard.calculateHardCandyCost();
			totalCost = cost - (calculateSoftCandySchoolProfit() + calculateHardCandySchoolProfit());
			return totalCost;
		}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		}